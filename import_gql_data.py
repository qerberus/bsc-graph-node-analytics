import gql
import os
import requests
import pandas as pd


def import_gql_data(
    api_uri,
    gql_client,
    influx,
    block,
    dest_measurement,
    tag_map,
    field_map,
):
    """Import the GQL data into InfluxDB.
    We need to set up a few internal convenience functions here.
    """

    def merge_dicts(dict1_, dict2):
        """Merge two dictionaries. (copied from stackoverflow)"""
        dict1 = dict1_.copy()
        if not isinstance(dict1, dict) or not isinstance(dict2, dict):
            return dict2
        for k in dict2:
            if k in dict1:
                dict1[k] = merge_dicts(dict1[k], dict2[k])
            else:
                dict1[k] = dict2[k]
        return dict1

    def map_to_query(map_, indent=0):
        """Transform a merged map into a GQL-friendly query."""
        i = " " * indent * 4
        query = str()
        for k, v in map_.items():
            if isinstance(v, dict):
                query += i + k + " {" + os.linesep
                query += map_to_query(v, indent + 1)
                query += i + "}" + os.linesep
            else:
                query += i + k + os.linesep
        return query

    def points_from_gql(ts, data):
        """Transform response data from GQL into an InfluxDB insert query.
        TODO: only 2-deep nested maps are supported
        """
        json_body = {
            "measurement": dest_measurement,
            "time": ts,
            "tags": {},
            "fields": {},
        }
        for k, v in tag_map.items():
            if isinstance(v, dict):
                for k2, v2 in v.items():
                    json_body["tags"][v2] = data[k][k2]
            else:
                json_body["tags"][v] = data[k]

        for k, v in field_map.items():
            if isinstance(v, dict):
                for k2, v2 in v.items():
                    json_body["fields"][v2] = float(data[k][k2])
            else:
                json_body["fields"][v] = float(data[k])

        return json_body

    # First merge the tap and field maps to query all the data from GraphQL
    merged_map = merge_dicts(tag_map, field_map)

    # We'll need to get the timestamp at this block, so ask the rpc api for that.
    block_params = {
        "jsonrpc": "2.0",
        "method": "eth_getBlockByNumber",
        "params": [hex(block), True],
        "id": 1,
    }

    # TODO: this might error out here. handle a possible exception
    response = requests.post(api_uri, json=block_params)
    ts = pd.Timestamp(int(response.json()["result"]["timestamp"][2:], 16), unit='s')

    # Get the pairs
    # TODO: make the query variable
    query = gql.gql(
        """
        query getPairs($block: Int!, $skip: Int!) {
            pairs(block: {number: $block}, first: 10, skip: $skip) {
        """
        + map_to_query(merged_map)
        + """
            }
        }"""
    )

    # Params for the query.
    # TODO: make "skip" 0 to start
    params = {
        "block": block,
        "skip": -10,
    }

    should_continue = True
    pairct = 0
    while should_continue:
        params["skip"] += 10
        result = gql_client.execute(query, variable_values=params)
        # TODO: what if we hit the end of the list exactly?
        #       will asking for more elements throw an error from GQL?
        pairct += len(result["pairs"])
        should_continue = len(result["pairs"]) == 10
        # write the data to InfluxDB
        influx.write_points(
            [points_from_gql(ts, pair) for pair in result["pairs"]], time_precision="s"
        )
