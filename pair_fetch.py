import gql
from influxdb import InfluxDBClient
from gql.transport.requests import RequestsHTTPTransport
from import_gql_data import import_gql_data

TAG_MAP = {
    "id": "id",
    "token0": {
        "id": "token0_id",
        "symbol": "token0_symbol",
        "name": "token0_name",
    },
    "token1": {
        "id": "token1_id",
        "symbol": "token1_symbol",
        "name": "token1_name",
    },
}

FIELD_MAP = {
    "reserve0": "reserve0",
    "reserve1": "reserve1",
    "reserveBNB": "reserveBNB",
    "reserveUSD": "reserveUSD",
    "totalSupply": "totalSupply",
    "token0Price": "token0Price",
    "token1Price": "token1Price",
    "volumeToken0": "volumeToken0",
    "volumeToken1": "volumeToken1",
    "volumeUSD": "volumeUSD",
    "txCount": "txCount",
}

DEST_MEASUREMENT = "test.cerberus.dex.cake"

START_BLOCK = 6780751


if __name__ == "__main__":
    transport = RequestsHTTPTransport(
        url="http://graph-node:8000/subgraphs/name/pancakeswap/exchange"
    )
    gql_client = gql.Client(transport=transport, fetch_schema_from_transport=True)

    influx_client = InfluxDBClient("influxdb", 8086, "root", "root", "dev")
    influx_client.create_database("dev")

    while True:
        import_gql_data(
            "http://bsc-archive:8545/",
            gql_client,
            influx_client,
            START_BLOCK,
            DEST_MEASUREMENT,
            TAG_MAP,
            FIELD_MAP,
        )
        START_BLOCK += 1
